---
layout: markdown_page
title: "Sales Qualification Questions"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The following questions can be used to help develop the opportunity and advance the buying decision.  These questions are grouped based on sales stages.

Target Buyer Persona: CIO/, CTO, VP of Application Development,  Product Owner, App Developer/DevOps Engineer
1. Qualification Stage
    * What led you to look at GitLab?
    * What are you currently using for continuous integration/continuous delivery (CI/CD)?
        - Example: Bamboo, Jenkins, Circle CI, Jetbrains.
    * Describe the projects that your dev teams are working on.
    * How would you describe your maturity level/progress with continuous integration and continuous delivery?
        - Is this something your group and company are looking to strive towards?
        - What has prevented this move so far?
    * How are you using CE right now, if applicable? check version.gitlab.com to identify if their company is on CE
    * What problems are they looking to solve?
        - If these problems are not solved, what happens?  What are the implications of these problems?
    * Please describe your DevOps stack. What tools are you currently using?
        - Bug Tracker (Issue Tracker)
        - CI
        - Code Review
        - Deployment
            - Containers
            - VMs
            - Microservices
            - Kubernetes    
        - Project Management/Agile Tools
    * What challenges (if any) are you experiencing with your current DevOps stack implementation and execution?
    * Where is this interest/initiative coming from?  (At the top or only this lead - is it company, unit, or personal interest.  The higher the initiative, the more likely to close and quickly)
    * What is the timing to purchase?
    * Do you need executive or senior level sponsorship to make a purchase?
    * How many individuals or teams are involved in the decision making process?
    * What are the requirements you have for this project?
        - What does success look like?
        - What will you be evaluating?
    * Are you considering other Git-based offerings?   If so, who?
    * Do you have a budget allocated to a solution?
    * What is your role and what do you do?
        - Ideal Roles: Infrastructure/Cloud/Enterprise Architects, Dev Ops, System Admin/Engineer, IT Managers/Directors, Release Engineer , VP Engineering
    * How would you describe what your group does?
    * How large is your group? (Goal is to find out who will use GitLab and how large the opportunity could be)
    * Are other groups using Git?
        - If no, why not? and what Version Control Systems are they using?
        - If yes, who are they using?
    * Does each team/group purchase their own solutions or is their a department who does this?
        - If a centralized department, what is that groups name?

1. Additional qualification questions if prospect signed up for free trial.  (Goal: Know how to ensure a successful trial)
    * How would you and your organization describe a successful trial?
    * What are the most important criteria in your evaluation?
    * How many users are you interested in deploying on GitLab?
    * Are you evaluating any other tools?
    * What is your timeframe for making a decision?
    * Are you looking to bring one division of your company on to EE?
        - If only a group, what is preventing you from bringing on the entire company at this time?

1. Opportunity Development Questions (Goal: develop their implicit and explicit needs.  Why they must make a decision)
    * How is security testing currently done in your organization?
    * What is the current process for finding vulnerabilities around Static Application Security Testing, Dependency Scanning, and/or License Management?
    * Do you have cloud-native initiatives? If so, what solutions are you exploring (i.e., Kubernetes)?
    * Describe your strategy for increasing the frequency of app deployments. What is the impact?
    * Where does high availability rank within your needs?
    * If competing against GitHub, review the battle card and discovery questions [here](https://docs.google.com/document/d/1xs3M7RypLNY3Vnuq2BkFBnr5ebo2CEwL9_N2_SYP7aQ/edit?usp=sharing).
    * If using Jira,
        - Describe your current implementation of Jira within your organization.
        - What are some of the challenges you have with Jira (if any)?
    * If selling against Jenkins, review the battle card and discovery questions [here](https://docs.google.com/document/d/1EMLShJ9-MLJIabWdbh4QJ4wiY2cf-ZbqYz4r0QsXRBU/edit?usp=sharing).
    * What is the financial impact on your organization/teams budget with other test and review tools?


1. Post Sale Account Development (Goal: Understand why they bought and use nuggets to further expand the sale and help with other sales)
    * How is the adoption of GitLab going?
    * What are you liking about GitLab EE?
    * What are you not liking or have questions about?
    * How has GitLab helped your group?  What are their use cases?
        - improve code quality?
        - speed up code releases?
    * How many projects do they have?
    * Is there anything you or your team would like to see within GitLab?
    * Are you seeing any challenges with not having other groups using GitLab as well?
    * Are there other groups that you think would benefit using GitLab?
        - If yes, how would you suggest getting in contact with them?
    * How many of the provisioned licenses are being used?
        - If low adoption, Is there anything I can do to help you?
        - If high adoption of licences, Ask what their growth plans are and when they feel they will need to increase their seats?
    * Would you be willing to be a reference customer for GitLab? If so, how has GitLab helped you and your organization?
