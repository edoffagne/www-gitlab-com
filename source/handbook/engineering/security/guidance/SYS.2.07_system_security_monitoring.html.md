---
layout: markdown_page
title: "SYS.2.07 - System Security Monitoring Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.2.07 - System Security Monitoring

## Control Statement

Critical systems are monitored in accordance to predefined security criteria and alerts are sent to authorized personnel. Confirmed incidents are tracked to resolution.

## Context

Having standards for security configurations and performance is useless without the ability to detect deviations from those standards. This control requires all critical systems to be monitored to ensure those systems are configured and performing the way we intend. If this monitoring identifies a security incident, this control also requires us to manage that incident fully until it is marked as resolved.

## Scope

This control applies to all production systems critical to the delivery of the GitLab SaaS product.

## Ownership

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.2.07_system_security_monitoring.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.2.07_system_security_monitoring.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.2.07_system_security_monitoring.md).

## Framework Mapping

* ISO
  * A.12.4.3
* SOC2 CC
  * CC3.2
  * CC3.3
  * CC3.4
  * CC4.2
  * CC5.1
  * CC5.2
  * CC6.1
  * CC7.2
  * CC7.3
* PCI
  * 10.2
  * 10.2.4
  * 10.5.5
  * 10.6
  * 10.6.1
  * 10.6.2
  * 10.6.3
  * 10.8.1
  * 12.10.5
