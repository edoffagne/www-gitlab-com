---
layout: markdown_page
title: "IT Ops"
---
# Welcome to the IT Ops Handbook

The IT Ops department is part of the [GitLab Business Ops](/handbook/business-ops) function that guides systems, workflows and processes and is a singular reference point for operational management.

## GET IN TOUCH

* [IT Ops Issues](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues)
* #it-ops on slack

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement

IT Ops will work with Security, PeopleOps and Business Operations to develop automated on-boarding and off-boarding processes. We will develop secure integrations between Enterprise Business Systems and with our Data Lake. We will develop tooling and process to facilitate end-user asset management, provisioning and tracking. We will work to build API Integrations from the HRIS to third party systems and GitLab.com. We triage IT related questions as they arise. We build and maintain cross-functional relationships with internal teams to champion initiatives. We will spearhead on-boarding and off-boarding automation efforts with a variety of custom API integrations, including GitLab.com and third-party resources, not limited to our tech-stack, with scalability in mind.


## Access requests

For information about the access request process, please refer to the [access request project](
https://gitlab.com/gitlab-com/access-requests).

### Baseline Entitlements

For all incoming team-members, access to systems is handled through on-boarding issues. If your on-boarding issue is complete and closed but you still need access to a system listed below, please create a [new access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new) and add a link to this page in lieu of manager approval:

#### Systems that all team-members should have access to

100% of team-members should have access to the following systems at the following levels of access as part of their work at GitLab. This list has been pre-approved so if any team-member needs access to these systems they can reach out directly to the [system admin(s)](https://about.gitlab.com/handbook/business-ops/#tech-stack) and request access based on this pre-approval.

| System Name | Business Purpose | System Role (What level of access) | [Data Classification](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html) |
|---|---|---|---|
| 1Password | User Password Management | Team Member | RED |
| BambooHR | Human Resource Platform | Employee | RED |
| Calendly | Add-in for meeting Scheduling | Employee | YELLOW |
| Carta | Shares Management | Employee | RED |
| CultureAmp | 360 Feedback Management | User | YELLOW |
| Expensify | Expense Claims and Management | Employee | ORANGE |
| GitLab.com | Gitlab Application for Staff | Employee | RED |
| Greenhouse | Recruiting Portal | Interviewer | RED |
| Gsuite | Email, Calendar, and Document sharing/collaboration | GitLab.com Org Unit | RED |
| Moo | Business Cards | User | YELLOW |
| NexTravel | Travel booking | Employee | ORANGE |
| Sertifi | Digital signatures, payments, and authorizations | User | YELLOW |
| Slack | GitLab async communications | Member | RED |
| Periscope Data | Data Analysis and Visualisation | User | RED |
| Will Learning | Staff Training and Awareness Portal | User | YELLOW |
| Zoom | For video conferencing / meetings | User | RED |

[Role-based baseline entitlements](https://about.gitlab.com/handbook/engineering/security/#baseline-role-based-entitlements-access-runbooks--issue-templates) have been established for the following roles:

1. Site Reliability Engineer - SRE
1. Security Engineer, Operations - SecOps
1. Security Engineer, Automation
1. FrontEnd Engineer
1. Backend Engineer

## Automated Group Membership Reports for Managers

If you would like to check whether or not a team-member is a member of a Slack or a G-Suite group, you can view the following automated group membership reports:

[G-Suite Group Membership Reports](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members)

[Slack Group Membership Reports](https://gitlab.com/gitlab-com/security-tools/report-slack-group-members)

## Two factor issues and requests for support

While we are well positioned to provide support to all teams, our focus and preference is always on larger projects. We ask that all efforts be made to self-resolve before contacting IT Ops for support.

Certain support items like 2FA resets are time sensitive but there are some things you can do to help yourself.
For 2FA related problems for your Gitlab accounts, please use your backup codes or try generating [new ones](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh).

Follow these steps to successfully set up 2fa for your [Google account](https://support.google.com/accounts/answer/185839?co=GENIE.Platform%3DDesktop&hl=en).

When you get a new device, before you get rid of the old one, make sure to set up your authenticator app on the new device. You will need to log into your accounts with the old device and disable 2fa for your accounts. Then delete the accounts off the old device and re-enable the 2fa on your accounts on the authenticator app on the new device. 

As a distributed team, our current standard coverage window is ~13:00-22:00 UTC. High volumes of issues being triaged can dictate the delay in response within that window. If the issue is extremely time sensitive and warrants escalation, use judgement on whether or not it can wait until ‘business hours’. Contact details can be found in slack for escalation.


### How to contact us, or escalate priority issues outside of standard hours:

Team members contact details can be found in slack profiles.

Slack is not ideal for managing priorities of incoming issues, so we ask that all such requests get sent to it-issues@ or create an issue at [IT Ops Issues](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues) and we will triage and address them as soon as we can.  All issues created in the servicedesk queue are public by default.

Privileged or private communications should be sent to itops@ where all new issues are private by default, visible only to the reporter and appropriate team members.

Screenshots and videos are very helpful when experiencing an issue, especially if there is an error message.

## Laptops

### Laptop Ordering Process

The laptop ordering process starts as soon as an offer is accepted by a candidate and the initial Welcome email is sent by PeopleOps. This email will include a link to the Notebook Order Form where the new team member will state their intent for obtaining or ordering hardware.

Team members that live in these countries can be serviced via the IT Laptop Ordering Process: 

USA, Canada, Japan, Mexico, UK/Ireland, France, Spain, Germany, Italy, Australia, Russia, Netherlands.  

Please note that we are adding supported countries to this list as we discover our ability to order in them.  You can test this by going to order a Macbook Pro from the regional Apple store, and seeing if they let you customize a build or alternately refer you to local retailers.  If the latter, see below.

Team members that do not live in these countries will need to procure their own laptop and submit for reimbursement.  If the team member desires financial assistance the Company will advance the funds to help facilitate the purchase (see Exception Processes below).

### Key Performance Indicators
KPI 99% of laptops will arrive prior to start date or 14 days from the date of order.

### Exception Processes

If you are in a region where we are not able to have a laptop delivered, and you need to request funds be advanced in order for a local purchase to take place ;
Obtain two quotes from local retailers (online or physical).

Email your manager with those quotes attached, requesting the funds advance and detailing the reason why (geo region, unable to have laptop delivered).
Your manager will then forward their approval to Wilson & Jenny in Finance for final approval and dispensation.

Should a laptop not be available to a new GitLab team-member upon their start date, but is pending, interim options include ;

    - Using personal non-windows hardware (mac, linux, mobile)
    - Renting and expensing non-windows hardware
    - Purchasing and expensing (or returning) a Chromebook

### Laptop Configurations

GitLab approves the use of Apple and Linux operating systems, Windows is prohibited.

Apple hardware is the common choice for most GitLab team-members, but if you are comfortable using and self-supporting yourself with Linux (Ubuntu usually) you may also choose from the Dell builds below.

Apple Hardware

- Everyone Else (Macbook) - [13” / 256gig SSD / 16gigs RAM / Quad-Core i5 CPU](https://www.apple.com/shop/buy-mac/macbook-pro/13-inch-space-gray-256gb-2.4ghz-quad-core-processor-with-turbo-boost-up-to-4.1ghz#)

- For Engineers, Support Engineers, Data Analysts, Technical Marketing Managers, UX Designers, UX Managers (Macbook) - [15”  / 512gig SSD / 32gig RAM / i9 or i7 CPU](https://www.apple.com/shop/buy-mac/macbook-pro/15-inch-space-gray-2.3ghz-8-core-processor-with-turbo-boost-up-to-4.8ghz-512gb#)

Linux Hardware

- ** For Engineers, Support Engineers, Data Analysts, Technical Marketing Managers, UX Designers, UX Managers (Dell/Linux) - [15”  / 512gig SSD / 32gig RAM / i9 or i7 CPU ](https://www.dell.com/en-us/work/shop/cty/pdp/spd/precision-15-5530-laptop/xctop5530hwus)

- Everyone Else (Dell/Linux) - [13” / 256gig SSD / 16gigs RAM / Quad-Core i5 CPU](https://www.dell.com/ng/business/p/latitude-13-7300-laptop/pd)

We strongly encourage Macs, but we do allow Linux if you are capable of self-support and updates.

For Linux laptops, we recommend purchasing a Dell computer pre-loaded with Ubuntu Linux. The reasons for using Dell for a Linux laptop are as follows:
* There are several manufacturers of laptop systems that offer Linux, but Dell is the only major manufacturer that has done so for years, and it has already worked out shipping issues for all of the countries where GitLab employees live.
* As we move forward with Zero Trust networking solutions, we need to have a stable and unified platform for deployment of software components in the GitLab environment. Standardization on a single platform for Linux simplifies this.
* Ubuntu 18.04 LTS is the preferred platform due to both stability and an extremely fast patch cycle - important for security patches.
* There are opportunities for corporate discounts in the future if we can concentrate purchases from a single vendor.
* To date, all of Dell's security issues have involved their use of Windows, not their hardware.

** NOTE : for this model, it is suggested to also purchase and expense (or request in your initial laptop order) an inexpensive webcam. The built in webcam looks straight up your nose. Also note that [1Password](/handbook/security/#1password-guide) does not yet have a native client for Linux, but [there is a browser extension](https://support.1password.com/getting-started-1password-x/). Max price: **the price of the equivalent Mac laptop**

Laptops are purchased by IT Ops when a team-member comes on board; the team-member will be sent a form to fill out for ordering.

### Laptop Refresh

Replacement laptops for broken GitLab laptops can be purchased as needed by [creating an issue](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/issues/new?issue) in the IT Ops issue tracker project and using the `repair_replace` template.

This process can also be followed for laptops that are not broken but old enough that you are having trouble completing your work. Please refer to the [spirit of spending company money](https://about.gitlab.com/handbook/spending-company-money/) when deciding whether or not it is appropriate to replace your functioning laptop. Everyone's needs are different so it is hard to set a clear timeline of when computer upgrades are necessary for all team-members, but team-members become eligible for an updated laptop after 3 years.

Many team members can use their company issued laptop until it breaks. If your productivity is suffering, you can request a new laptop. The typical expected timeframe for this is about three years, but it can depend on your usage and specific laptop.  Laptops paid for by the company are property of GitLab and need to be reported with serial numbers, make, model, screen size and processor to People Ops by adding it to this form: [GitLab laptop information](https://docs.google.com/forms/d/1XcuK5oZJkdDjt4VE7xYHXPAAb91Dsz8Bsm0ZFJ8mXck/prefill) for proper [asset tracking](/handbook/finance/accounting/#asset-tracking). Since these items are company property, you do not need to buy insurance for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care), but you do need to report any loss or damage to PeopleOps as soon as it occurs. Links in the list below are to sample items, other options can be considered.

### Configuring New Laptops

New laptops should be configured with security in mind. Please refer to [security best practices](https://about.gitlab.com/handbook/security/#best-practices) when configuring new laptops. **All team-members must provide proof of whole-disk encryption within the new laptop order issue.**

### Returning Old Laptops

Part of the IT Ops replacement laptop process is providing each team-member with instructions about how to return their old laptop (whether outdated or broken). All laptops must be returned **within 2 weeks of receiving the replacement laptop**, so please prioritize transferring information between laptops within this timeframe.

All team-member laptops must be securely erased before being returned. This not only protects the company, but also protects you since it is possible for personal information to exist on these machines. Reformatting a computer is not sufficient in these cases because it is possible for sensitive data to be recovered after reinstalling an operating system.

## Other Resources

### Okta

In an effort to secure access to systems, GitLab is utilizing Okta. The key goals are:

- We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorized connections to key assets with a greater degree of certainty.
- We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
- We can better manage the Provisioning and De-provisioning process for our users to access these application, by use of automation and integration into our HRIS system.
- We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.

To read more about Okta, please visit the [Okta](https://about.gitlab.com/handbook/business-ops/okta/) page of the handbook.

### Full Disk Encryption

To provide proof of Full Disk Encryption, please do the following depending on the system you are running.

- Apple : Take a screenshot showing both the confirmation of enabled Full Disk Encryption, as well as the About Your Mac info showing your serial #.
- Linux : Take screenshot the output of ``sudo dmsetup ls && sudo dmidecode -s system-serial-number && cat /etc/fstab``