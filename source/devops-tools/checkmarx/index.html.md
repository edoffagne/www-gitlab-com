---
layout: markdown_page
title: "Checkmarx"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Both Checkmarx and GitLab Ultimate offer open source component scanning along with Static Application Security Testing. Checkmarx offers IAST instead of DAST and does not offer container scanning.  
  
The Checkmarx [vision](https://www.youtube.com/watch?v=gTGvl_V5rOA&feature=youtu.be) is closest to GitLab among the app sec vendors. But because they must integrate into the rest of the SDLC via APIs, their path toward execution is more limited. However, like the other app sec vendors, Checkmarx is expensive. It is priced per developer with a rough estimate of 12 Developers for $59k USD per year or 50 Developers for $99k USD per year. Checkmarx uses Whitesource for dependency scanning and charges an extra $12k USD per year for this open source scanning.  
    
Checkmarx excels in that they are context aware, meaning they can mark what is not exploitable based on path. GitLab lacks this capability.  
  
GitLab automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management. All of this is part of the single GitLab Ultimate application.

## Strengths
* Gartner Magic Quadrant leader in Application Security Testing for 2018

## Resources
* [Checkmarx website](https://www.checkmarx.com/)

## Comparison
