---
layout: markdown_page
title: Product Vision - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

_"When technology delivers basic needs, user experience dominates." _  - Donald Norman

This is the product vision for Fulfillment. Fulfillment manages a variety of categories that are important for GitLab's ongoing success such as Licensing, Transactions and Telemetry.

| Category | Description |
| ------ |  ------ |
| [Licensing](/direction/fulfillment/licensing) | How we fulfill our users purchases of GitLab Plans through license key distribution. |
| [Transactions](/direction/fulfillment/transactions) | How our users purchase GitLab Plans and add-ons, and how they manage their account information. |
| [Telemetry](/direction/fulfillment/telemetry) | Insights into how users use our product. |

The overall vision for Fulfillment is to provide GitLab’s customers with an invisible user experience when handling their license keys and managing their purchases. In addition, Fulfillment looks after GitLab's product usage data framework, called Telemetry data, which enables GitLab's Product team to understand user needs and prioritize accordingly.

Billing, account management and handling license keys can be confusing, boring and time-consuming. We want these things to be easy and frictionless so you can focus on enjoying our product.

We also want to provide our internal teams with valuable and insightful data. Improving how we collect and use product usage data is the first and most crucial stepping stone to visualising our data so we can make the best decisions for our users and therefore our business. We also want Telemetry data to be a gateway to providing great meta features to users. Here are some examples of these:

* License key synchronisation so you never have to worry about managing your license keys again
* The ability to raise support tickets effortlessly from within GitLab itself which includes an automatic snapshot of what happened in the last few minutes
* Access to a fair billing model by sending regular user activity data to GitLab's license servers

If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to Luca Williams via [e-mail](mailto:luca@gitlab.com),
[Twitter](https://twitter.com/tipyn2903), or by [scheduling a video call](https://calendly.com/tipyn).

<!-- >## What's next

#### Immediate priorities

#### Next up -->

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!
