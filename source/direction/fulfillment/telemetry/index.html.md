---
layout: markdown_page
title: "Category Vision - Telemetry"
---

- TOC
{:toc}

## 🔮 Telemetry
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include use cases, personas,
and user journeys into this section. -->

* [Overall Vision](https://about.gitlab.com/direction/fulfillment)
* [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Atelemetry)
* [Maturity: Minimal](/direction/maturity/)
* [Documentation](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html)
* [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Atelemetry)

#### A brief history of usage ping

Historically, GitLab has utilised a primitive and limited product usage tracking system (developed in-house) called usage ping. This consists primarily of _counts_, e.g. how many pipelines, issues, projects etc. that a user has in their instance/group. A snapshot of this data is sent once a week to GitLab's [Version application](https://gitlab.com/gitlab-org/version-gitlab-com) (which is also managed by Fulfillment), this ping also includes a version check to understand what version of GitLab is currently implemented.

The above is where we are today, but we need to move towards becoming a more data-informed Product organisation at GitLab which requires a much more robust system for product usage data collection.

Additional Links:

* [Feature Instrumentation](https://about.gitlab.com/handbook/product/feature-instrumentation/)
* [usage_data.rb](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/lib/gitlab/usage_data.rb)

Please reach out to  Luca Williams, Product Manager for Fulfillment ([E-Mail](mailto:luca@gitlab.com)/
[Twitter](https://twitter.com/tipyn2903)) if you'd like to provide feedback or ask
questions about what's coming.

### 🤔 What is Telemetry, Anyway?

[Telemetry](https://en.wikipedia.org/wiki/Telemetry) is an automated communications process by which measurements and other data are collected at remote or inaccessible points and transmitted to receiving equipment for monitoring. The word is derived from Greek roots: tele = remote, and metron = measure. In software, Telemetry data records clicks, counts, time spent on tasks, webpages, growth in the usage of a feature over time, trends of all these things, etc.

Telemetry data is essential to building a successful product. It provides insights into how users interact with and use a product and allow many functions within a company to make educated decisions. To build an efficient, well-considered product with a great user experience, Product Managers need insights on how the decisions they previously made are impacting users. It allows them to understand what matters to users and what doesn't and helps them deliver high quality, low effort interactions that users love. ❤️

Telemetry is sometimes viewed through a negative lens. Privacy is becoming more and more of a serious topic in recent years due to the rise in social media, [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) and the increasing need for more secure applications to protect users. However, Telemetry is not just a term used in software, it is commonly used in medicine, science, meteorology and other similar areas. There is even such a job as a Telemetry Nurse, which is a specialised role in the medical field where Nurses monitor patients vitals, record and interpret the data and provide this to Doctors to show how a patient is recovering, allowing the Doctor to prescribe a more effective and care-driven treatment plan.

This is not so different from software, really.

Telemetry data doesn't just make Product Managers lives easier; It's also hugely beneficial to Customer Success teams by allowing them to see where users may need additional help or support. It enables Customer Success Managers and Technical Account Managers to proactively reach out and make sure that users are happy with what they've purchased. It's equally beneficial to teams such as Product Marketing and Sales by helping them know what story to tell future users of the product and what might be useful to current users to keep them up to date and well informed.

## 🔭 Vision

Data is becoming more and more important for building products. Unfortunately, it's tricky to implement usage tracking, and often requires either complex and confusing code/integrations, or expensive, proprietary tools (or both).

Everyone should be able to easily track how their users are using their product whether they are a small team working on an Android app or a vast, enterprise company. Our goal is to build an open source Telemetrics API that anyone can easily integrate into their product to help them make good decisions.

#### This API should be:

- Efficient, predictable and consistent: it should be easy for engineers to interact with and re-use code.
- Simple, minimal and clear: the code should be lightweight, beautifully written with excellent documentation and should always be reliable.
- Scalable, flexible and app agnostic: it should be easy to maintain and update, with low impact to end-user developers.

Our users should feel safe and protected while opting in to sending GitLab Telemetry data. They should know that their data is being used respectfully and only to further improve our products and make them more lovable. As always at GitLab, Transparency is a [core value](https://about.gitlab.com/handbook/values/#transparency) and this includes Telemetry. Users should always know what we are collecting and how, where and when we are using it.

Telemetry should also be a feature for our users, providing them with a great user experience through unique insights and tools. We aim to utilise the power and capabilities of Telemetry data to enrich _everyone's_ lives, not just the people who are collecting and analysing the data.

Design and user experience is an overall priority for Fulfillment. Please visit this [meta epic](https://gitlab.com/groups/gitlab-org/-/epics/1176) to see ongoing design and UX improvements to the Fulfillment group's categories.

## 🎭 Target audience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

We want to build the Telemetry API with significant empathy towards Data Teams and Software Engineers. It's essential that the data output of working with this API  be of high integrity, and the API itself should require minimal effort to implement. So much so that it could quickly become a necessary step of merging a new feature into production: i.e. Does this have Telemetry tracking included? Check.

### Sasha (Software Developer) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)

* 🙂 **Minimal** -
* 😊 **Viable** -
* 😁 **Complete** -
* 😍 **Lovable** -

### Dana (Data Analyst) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#dana-data-analyst)

* 🙂 **Minimal** -
* 😊 **Viable** -
* 😁 **Complete** -
* 😍 **Lovable** -

For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

## 🚀 What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

We have some short term wins listed in [🍎 Low hanging Telemetry fruit](https://gitlab.com/groups/gitlab-org/-/epics/942), which will improve the user experience and performance of usage ping. Usage ping is what we have today, so it is important to put in some low effort for high gains that can tide us over as we iterate towards the larger vision.

<!--  ## 🏅 Competitive landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- ## Analyst landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ## Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ## 🎢 Top user issues
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## 🦊 Top internal customer issues/epics
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

* [Usage ping data framework discussion](https://gitlab.com/gitlab-org/gitlab-ce/issues/53942)
* [Collect telemetry and metrics directly in GitLab](https://gitlab.com/gitlab-org/gitlab-ee/issues/10158)
* [🍎 Low hanging Telemetry fruit](https://gitlab.com/groups/gitlab-org/-/epics/942)
* [Category issues listed by popularity](https://gitlab.com/gitlab-org/gitlab-ee/issues?label_name%5B%5D=group%3A%3Atelemetry&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

<!-- ## Top Vision Item(s)
What's the most important thing to move your vision forward?-->

## 📚 Notable reading

* <https://docs.microsoft.com/en-us/azure/azure-monitor/app/api-custom-events-metrics>
* <https://clearlinux.org/documentation/clear-linux/guides/telemetrics/telem-guide>
* <https://developer.kontakt.io/backend/le/telemetry/configuration/>
* <https://www.mapbox.com/telemetry/>
* <https://conversionxl.com/blog/product-analytics/>
